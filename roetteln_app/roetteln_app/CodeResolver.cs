﻿using System.Collections.Generic;

namespace roetteln_app
{
    class CodeResolver
    {
        private Dictionary<string, string> _codesToVideoPaths = new Dictionary<string, string>
        {
            {"kueche", "file:///android_asset/kueche.mp4" },
            {"saal", "file:///android_asset/saal.mp4"},
            {"torturm", "file:///android_asset/torturm.mp4" },
            {"roetteln_1300", "file:///android_asset/roetteln_1300.mp4" },
            {"roetteln_1500", "file:///android_asset/roetteln_1500.mp4" },
            {"blide", "file:///android_asset/blide.mp4" },
            {"bombarde", "file:///android_asset/bombarde.mp4" },
            {"riesenbombarde", "file:///android_asset/riesenbombarde.mp4" },
            {"rammbock", "file:///android_asset/rammbock.mp4" },
            {"schutzwand", "file:///android_asset/schutzwand.mp4" },
            {"roetteln_heli", "file:///android_asset/roetteln_heli.mp4" }
        };

        public string ResolveCode(string code)
        {
            string pathToVideo;

            try
            {
                pathToVideo = _codesToVideoPaths[code];
            }
            //Return an error code if no video URI is found
            catch
            {
                pathToVideo = "-1";
            }

            return pathToVideo;
        }
    }
}
