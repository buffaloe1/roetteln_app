﻿using System;
using System.ComponentModel;
using Xamarin.Forms;
using ZXing.Net.Mobile.Forms;

namespace roetteln_app
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        private ZXingScannerPage _scannerPage;
        private CodeResolver _codeResolver;

        public MainPage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);

            _codeResolver = new CodeResolver();
            _scannerPage = new ZXingScannerPage();

            _scannerPage.OnScanResult += (result) =>
            {
                // Stop scanning
                _scannerPage.IsScanning = false;

                // Pop the page and show the result
                Device.BeginInvokeOnMainThread(async () =>
                {
                    await Navigation.PopAsync();
                    ProcessBarcode(result.Text);
                });
            };
        }

        public async void ScanButtonClicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(_scannerPage);
        }

        private async void ProcessBarcode(string barcode)
        {
            string pathToVideo = _codeResolver.ResolveCode(barcode);

            if(pathToVideo == "-1")
            {
                await DisplayAlert("Fehler", "Bitte güligen Code scannen", "OK");
                return;
            }

            await Navigation.PushAsync(new VideoPage(pathToVideo));
        }

    }
}
