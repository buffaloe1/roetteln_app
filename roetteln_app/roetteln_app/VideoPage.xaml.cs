﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using MediaManager;
using System;

namespace roetteln_app
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class VideoPage : ContentPage
    {
        private string _pathToVideoFile { get; set; }

        public VideoPage(string pathToVideoFile)
        {
            InitializeComponent();
            _pathToVideoFile = pathToVideoFile;
        }

        protected async override void OnAppearing()
        {
            base.OnAppearing();

            try
            {
                await CrossMediaManager.Current.Play(_pathToVideoFile);
            }
            catch (Exception e)
            {
                await DisplayAlert("Ausnahme aufgetreten", e.Message, "OK");
            }
        }

        protected async override void OnDisappearing()
        {
            base.OnDisappearing();

            await CrossMediaManager.Current.Stop();
        }
    }
}